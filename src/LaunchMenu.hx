package;

import AchievementSystem.Achievement;
import ceramic.Scene;
import ceramic.Quad;
import ceramic.Text;
import ceramic.Color;
import ceramic.Border;

using ceramic.Easing;

class LaunchMenu extends Scene {
    var title: Text;
    var startText: Text;
    var bg: Background;
    var startTextPos: Float;
    override function preload() {
        assets.add(Fonts.FONTS__ROBOTO_BLACK);
        assets.add(Fonts.FONTS__ASAP_ITALIC_VYZ_O);
    }

    override function create() {
        bg = new Background(this);
        add(bg);

        startTextPos = height * 0.8;

        title = new Text();
        title.font = assets.font(Fonts.FONTS__ROBOTO_BLACK);
        title.content = "A Pawn's Sacrifice";
        title.pointSize = Project.titleSize;
        title.anchor(0.5, 0.5);
        title.pos(width/2, height * 0.4);
        title.color = Color.BLACK;
        title.component('rotateAndBulge', new RotateBulge());
        add(title);

        var start = new Quad();
        start.size(width, Project.subtitleSize + 16);
        start.color = 0xFF1C2F;
        start.alpha = 0.75;
        start.anchor(0.5, 0.5);
        start.pos(width/2, startTextPos);
        add(start);

        var startBorder = new Border();
        startBorder.borderColor = Color.BLACK;
        startBorder.borderSize = 3.5;
        startBorder.size(start.width, start.height);
        startBorder.borderLeftSize = 0;
        startBorder.borderRightSize = 0;
        start.add(startBorder);
        
        startText = new Text();
        startText.font = assets.font(Fonts.FONTS__ASAP_ITALIC_VYZ_O);
        startText.content = "Press Enter to Sacrifice";
        startText.pointSize = Project.subtitleSize;
        startText.anchor(0.5, 0.5);
        startText.pos(start.x, start.y);
        startText.color = Color.BLACK;
        startText.clip = start;
        add(startText);

        AchievementSystem.loadAchievements();
        var ach = AchievementSystem.addAchievement(Achievement.START_GAME, assets.font(Fonts.FONTS__ASAP_M2PR));
        add(ach);
        
        input.onKeyUp(this, keyUp);
    }
    override function update(d: Float) {

        bg.update(d); //update background
        
        startText.x += 70 * d;
        if (startText.x >= width + (startText.width/2)) {
            startText.x = -startText.width/2; //return text to beginning when it goes off screen
        }
    }

    function keyUp(key: ceramic.Key) {
        if (key.scanCode == ceramic.ScanCode.ENTER) {
            Transition.fadeIn(this, new MainMenu());
        }
    }
}

class RotateBulge extends ceramic.Entity implements ceramic.Component {
    @entity var v:Text;
    var startRotation = 0;
    var endRotation = 30;
    var startSize = 50;
    var endSize = 55;
    var duration = 0.7;
    function bindAsComponent() {
        rotateAndBulge();
    }
    function rotateAndBulge() {
        v.tween(QUAD_EASE_IN_OUT, duration, startRotation, endRotation, (value, t) -> {
            v.rotation = value;
        });
        v.tween(QUAD_EASE_IN_OUT, duration, startSize, endSize, (value, t) -> {
            v.pointSize = value;
        }).onceComplete(this, () -> {
            v.tween(QUAD_EASE_IN_OUT, duration, endRotation, startRotation, (value, t) -> {
                v.rotation = value;
            });
            v.tween(QUAD_EASE_IN_OUT, duration, endSize, startSize, (value, t) -> {
                v.pointSize = value;
            }).onceComplete(this, () -> {
                rotateAndBulgeBack();
            });
        });
    }
    function rotateAndBulgeBack() {
        v.tween(QUAD_EASE_IN_OUT, duration, startRotation, -endRotation, (value, t) -> {
            v.rotation = value;
        });
        v.tween(QUAD_EASE_IN_OUT, duration, startSize, endSize, (value, t) -> {
            v.pointSize = value;
        }).onceComplete(this, () -> {
            v.tween(QUAD_EASE_IN_OUT, duration, -endRotation, startRotation, (value, t) -> {
                v.rotation = value;
            });
            v.tween(QUAD_EASE_IN_OUT, duration, endSize, startSize, (value, t) -> {
                v.pointSize = value;
            }).onceComplete(this, () -> {
                rotateAndBulge();
            });
        });
    }
}