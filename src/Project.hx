package;

import ceramic.Entity;
import ceramic.Color;
import ceramic.InitSettings;

class Project extends Entity {

    static public final titleSize = 50;
    static public final subtitleSize = 30;

    function new(settings:InitSettings) {

        super();

        settings.antialiasing = 2;
        settings.background = 0xA9F7CC;
        settings.targetWidth = 840;
        settings.targetHeight = 680;
        settings.scaling = RESIZE;
        settings.resizable = false;
        settings.title = "A Pawn's Sacrifice";
        settings.defaultFont = Fonts.FONTS__ASAP_M2PR;

        app.onceReady(this, ready);

    }

    function ready() {
        app.onTerminate(app, () -> {
            AchievementSystem.stop();
        });
        // Set MainScene as the current scene (see MainScene.hx)
        app.scenes.main = new LaunchMenu();
    }

}
